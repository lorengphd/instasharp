﻿using InstaSharp.Models.Responses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace InstaSharp.Tests
{
    public class TestBase
    {
        protected readonly OAuthResponse Auth;
        protected readonly InstagramConfig Config;
        protected readonly InstagramConfig ConfigWithSecret;

        protected TestBase()
        {
            // test account client id
            Config = new InstagramConfig()
            {
                ClientId = "f421a3063d6649e78f96d36708741adf"
            };

            ConfigWithSecret = new InstagramConfig()
            {
                ClientId = "f421a3063d6649e78f96d36708741adf",
                CallbackUri = "http://www.lorenanderson.com",
                ClientSecret = "79504f1602d548f293aad54075293299"
            };


            // dummy account data. InstaSharpTest
            Auth = new OAuthResponse()
            {
                //https://www.instagram.com/oauth/authorize/?client_id=f421a3063d6649e78f96d36708741adf&amp;redirect_uri=http://www.lorenanderson.com&amp;response_type=token&amp;scope=public_content+likes+follower_list
                AccessToken = "2372508994.f421a30.2b897889f62740f89d699a8dabf5e853",
                User = new Models.UserInfo { Id = 2372508994 }
                // lorenanderson.photo 2372508994
            };
        }
        protected static void AssertMissingClientSecretUrlParameter(Response result)
        {
            Assert.AreEqual(HttpStatusCode.BadRequest, result.Meta.Code);
            Assert.AreEqual("Missing client_secret URL parameter.", result.Meta.ErrorMessage);
            Assert.AreEqual("OAuthClientException", result.Meta.ErrorType);
        }
    }
}
